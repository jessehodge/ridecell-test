# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from .models import ParkingSpots


# Create your tests here.


class ParkingSpotTests(APITestCase):
    def test_create_parking_spot(self):
        """
        Ensure we can create a new parking spot object.
        """
        url = reverse('parking-spot-list')
        data = {
          "latitude": 1.234,
          "reserved": False,
          "longitude": 2.345
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ParkingSpots.objects.count(), 1)
        self.assertEqual(ParkingSpots.objects.get().id, 1)

    def test_reserve_parking_spot(self):
        """
        Ensure we can reserve a parking spot.
        For some reason it's saying the url is 404.
        """
        url = reverse('parking-spot-detail', kwargs={'pk': '1'})
        data = {
            "reserved": True
        }
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_parking_spot_search(self):
        """
        Ensure that we can search for a parking spot
        This test is also saying it can't find the url.
        I would want to implement maybe a different test method.
        """
        url = 'api/v1/search/1/2/3/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)