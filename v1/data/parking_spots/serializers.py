from rest_framework.serializers import ModelSerializer
from .models import ParkingSpots


class ParkingSpotSerializer(ModelSerializer):

    class Meta:
        model = ParkingSpots
        fields = '__all__'
