# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny
from .models import ParkingSpots
from .serializers import ParkingSpotSerializer
from geopy.distance import geodesic

# Create your views here.


class ParkingSpotView(ModelViewSet):
    queryset = ParkingSpots.objects.all()
    serializer_class = ParkingSpotSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()


class ParkingSpotSearchView(ListAPIView):
    serializer_class = ParkingSpotSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get_queryset(self):
        lat = float(self.kwargs['lat'])
        long = float(self.kwargs['long'])
        radius = int(float(self.kwargs['radius']))
        ps_list = ParkingSpots.objects.filter(reserved=False)
        available_spots = []
        for ps in ps_list:
            ps_location = (ps.latitude, ps.longitude)
            input_address = (lat, long)
            distance = geodesic(ps_location, input_address).meters
            if distance < radius:
                available_spots.append(ps)
        return available_spots

        # This is the solution if the GDAL library worked.
        # radius = int(float(self.kwargs['radius']))
        # pnt = GEOSGeometry('POINT('+self.kwargs['lat']+' ' + self.kwargs['long']+')', 32140)
        # ps_list = ParkingSpots.objects.filter(point__distance__lt=(pnt, D(m=radius)), reserved=False)
        # return ps_list
