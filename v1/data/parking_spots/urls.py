from rest_framework_bulk.routes import BulkRouter
from django.conf.urls import url
from .views import ParkingSpotView, ParkingSpotSearchView

router = BulkRouter()

router.register('parking_spot', ParkingSpotView, base_name="parking-spot")

urlpatterns = [
    url('^search/(?P<lat>.+)/(?P<long>.+)/(?P<radius>.+)/$', ParkingSpotSearchView.as_view())
]

urlpatterns += router.urls
