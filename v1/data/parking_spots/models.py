# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class ParkingSpots(models.Model):
    reserved = models.BooleanField(default=False)
    latitude = models.FloatField(max_length=20)
    longitude = models.FloatField(max_length=20)
