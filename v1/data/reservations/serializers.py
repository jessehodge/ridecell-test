from rest_framework.serializers import ModelSerializer
from .models import Reservations


class ReservationSerializer(ModelSerializer):

    class Meta:
        model = Reservations
        fields = '__all__'
