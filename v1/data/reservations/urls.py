from rest_framework_bulk.routes import BulkRouter
from django.conf.urls import url
from .views import ReservationViewSet

router = BulkRouter()

router.register('reservation', ReservationViewSet, base_name="reservation")

urlpatterns = []

urlpatterns += router.urls