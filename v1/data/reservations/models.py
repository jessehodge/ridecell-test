# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from ..auth.models import RidecellUser
from ..parking_spots.models import ParkingSpots

# Create your models here.


class Reservations(models.Model):
    reserved_by = models.ForeignKey(RidecellUser, on_delete=models.CASCADE)
    parking_spot = models.ForeignKey(ParkingSpots, on_delete=models.CASCADE)
    length_of_reservation = models.FloatField()
    # format issues
    started_reservation = models.CharField(max_length=20, blank=True, null=True)
    ended_reservation = models.CharField(max_length=20, blank=True, null=True)
    actual_time_reserved = models.FloatField(blank=True, null=True)