# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import Reservations
from .serializers import ReservationSerializer
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from datetime import timedelta, datetime

# Create your views here.


class ReservationViewSet(ModelViewSet):
    queryset = Reservations.objects.all()
    serializer_class = ReservationSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def update(self, request, *args, **kwargs):
        end_reservation = datetime.strptime(request.data['ended_reservation'], '%Y-%m-%dT%H:%M:%S')
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        start_reservation = datetime.strptime(instance.started_reservation, 'YYYY-MM-DDDDT%H:%M:%S')
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        if end_reservation:
            time_difference = abs((end_reservation - start_reservation).seconds)
            print(time_difference)
            serializer.data['actual_time_reserved'] = time_difference
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)
