# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, UserManager

# Create your models here.


class RidecellUser(AbstractBaseUser):

    USER_TYPES = (
        ('parker', 'Parker'),
        ('manager', 'Manager'),
    )

    user_type = models.CharField(max_length=200, choices=USER_TYPES)
    email = models.EmailField(unique=True)

    REQUIRED_FIELDS = ['password']
    USERNAME_FIELD = 'email'

    objects = UserManager()