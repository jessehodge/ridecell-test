# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import parsers, renderers
from rest_framework.authtoken.models import Token
from .serializers import AuthCustomTokenSerializer

from .serializers import RidecellUserSerializer
from .models import RidecellUser


class RegisterView(CreateAPIView):
    """
    Register View
    """
    queryset = RidecellUser.objects.all()
    serializer_class = RidecellUserSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()


class ObtainAuthToken(APIView):
    permission_classes = [AllowAny]
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthCustomTokenSerializer
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_type': user.user_type,
            'user_email': user.email,
            'user_id': user.id
            })


obtain_auth_token = ObtainAuthToken.as_view()