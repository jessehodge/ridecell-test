from django.conf.urls import url
from .views import RegisterView, obtain_auth_token

urlpatterns = [
    url(r'^auth/register/?$', RegisterView.as_view(), name='register'),
    url(r'^auth/login', obtain_auth_token)
]
