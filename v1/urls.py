from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .data.auth import urls as auth_urls
from .data.parking_spots import urls as parking_spot_urls
from .data.reservations import urls as reservation_urls

router = DefaultRouter()

urlpatterns = [
    url(r'^', include(auth_urls)),
    url(r'^', include(parking_spot_urls)),
    url(r'^', include(reservation_urls))
]